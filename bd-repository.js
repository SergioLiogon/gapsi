class BdRepository {
    constructor(bdService) {
        this.bdService = bdService;
    }

    findAll() {
        return this.bdService.findAll();
    }

    save(supplier) {
        return this.bdService.save(supplier);
    }

    removeByName(name) {
        return this.bdService.removeByName(name);
    }
}

module.exports = BdRepository;