const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');

// Initialization
const app = new Koa()
const router = new Router()
const supplierRoutes = require('./suppier-routes')

// define routes
router.get('/', (ctx)=>{
    ctx.body = "Hello world"
})
router.get('/meta', (ctx) => {
    ctx.body = {
        version : '0.2',
        user: 'Sergio Liogon'
    }
})
app.use(cors());
app.use(bodyParser());
//routes injection routes
app.use(router.routes())
app.use(supplierRoutes.routes())


// allow use all http methods
app.use(router.allowedMethods())

// define port
const port = 9898
app.listen(port, ()=>{ 
    console.log(`App running on port ${port}`)
})