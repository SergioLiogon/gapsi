class Supplier {
    constructor(id, name, internalNumber, address){ 
        this.id = id
        this.name = name
        this.internalNumber = internalNumber
        this.address = address
    }
}

module.exports = Supplier
