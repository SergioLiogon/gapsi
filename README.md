# Proyecto Node.js con Koa y Next.js

Este proyecto utiliza Node.js con Koa en el backend y Next.js en el frontend.

## Backend

### Instalación

Para ejecutar el backend, sigue estos pasos:

1. Abre una terminal en la ruta raiz.
2. Ejecuta el siguiente comando para instalar las dependencias:

```bash
   npm install
   ```
Inicia el servidor de desarrollo con:
 ```bash
   npm run dev
 ```
El servidor backend estará disponible en http://localhost:9898.


## Frontend

### Instalación

Para ejecutar el frontend, sigue estos pasos:


1. Navega al directorio `frontend/gapsi-app` en una terminal.
2. Ejecuta el siguiente comando para instalar las dependencias:


```bash
   npm install
   ```
Inicia el servidor de desarrollo con:
 ```bash
   npm run dev
 ```
La aplicación frontend estará disponible en http://localhost:3000.

## Funcionalidades

### Frontend desarrollado en React/Next.js utilizando Material UI como biblioteca de componentes.
### Patrones de diseño utilizados: Singleton y Repository Pattern (ver archivos bd.service y bd-repository).
### Koa se utiliza para el servicio API en el backend.

#### Se adjunta en el correo una colección de Postman para realizar pruebas y revisiones.

