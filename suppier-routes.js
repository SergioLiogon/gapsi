const Router = require('koa-router');
const router = new Router()
const Supplier = require('./supplier');
const BdService = require('./bd.service');
const BdRepository = require('./bd-repository');
const bdService = BdService;
const bdRepository = new BdRepository(bdService);
router.get('/supplier/all', (ctx)=>{
    const suppliers = bdRepository.findAll()
    ctx.body = suppliers
})
router.get('/supplier/:id' , (ctx)=>{
    const { id } = ctx.params
    ctx.body = `Supplier info ${id}`
})

router.post('/supplier/save', (ctx)=>{
    //Parse request body to destructured data
    const { name, internalNumber, address} = ctx.request.body

    // Check if all fields are included
    if(!name || !internalNumber || !address) { 
        ctx.status = 400
        ctx.body = "All Fields Requiered!"
        return
    }
    // Parse request fields to Supplier Model
    const supplier = new Supplier("", name, internalNumber, address )

    //Call Save service in file supplier-service.js
    const response = bdRepository.save(supplier)
    if(response === 'DUPLICATE'){
        ctx.status = 500
        ctx.body = "Duplicate element"
        return
    }else{
        //Return HttpsStatus.OK and message
        ctx.status =  200
        ctx.body = "Success"
        return
    }
    
})

router.delete('/supplier/:name', (ctx)=>{
    const { name } = ctx.params
    const result = bdRepository.removeByName(name)
    console.log(result)
    console.log("Are u printing the result?")
    if(result){
        ctx.status = 200
        ctx.body = "Success"
    }else{
        ctx.status = 500
        ctx.body = "Element not found"
    }
})

module.exports = router