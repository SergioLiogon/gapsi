"use client";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";
import { useEffect, useState } from "react";
import axios from "axios";
import { Grid, Button, Alert, AlertTitle } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import TextField from '@mui/material/TextField';
import DeleteIcon from '@mui/icons-material/Delete';
function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function CustomPaginationActionsTable() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [suppliersResults, setSuppliersResults] = useState([]);
  const [open, setOpen] = useState(false);
  const [successToast, setSuccessToast] = useState(false)
  const [errorToast, setErrorToast] = useState(false)
  const [newSupplier, setNewSupplier] = useState({
    name: "",
    internalNumber: "",
    address: "",
  });
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  useEffect(() => {
    fetchData();
  }, []); //Only once

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewSupplier((prevSupplier) => ({
      ...prevSupplier,
      [name]: value,
    }));
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const fetchData = async () => {
    try {
      const response = await axios.get(`http://localhost:9898/supplier/all`);
      setSuppliersResults(response.data);
    } catch (error) {
      console.error("Error", error);
    }
  };
  const handleAddSupplier = async() => {
    try { 
      const response = await axios.post('http://localhost:9898/supplier/save', newSupplier)
      if(response.status === 200){
        showSuccessToast()
        fetchData()
      }else{
        showErrorToast()
      }
    }catch(error){
      console.error(error)
      showErrorToast()
    }
    // Clean data
    setNewSupplier({
      name: "",
      internalNumber: "",
      address: "",
    });
    
    handleClose();
  };

  const deleteItem = async(name)=>{
     try { 
      const response = await axios.delete(`http://localhost:9898/supplier/${name}`)
      if(response.status === 200){
        fetchData()
        showSuccessToast()
      }else{
        showErrorToast()
      }
     }catch(error){
      console.error(error)
      showErrorToast()
     }
  }
  const showSuccessToast = () => {
    setSuccessToast(true)
    setTimeout(()=>{
      setSuccessToast(false)
    },2000)
  }
  const showErrorToast = () => {
    setErrorToast(true)
    setTimeout(()=>{
      setErrorToast(false)
    },2000)
  }
  return (
    <>
      <Grid
        container
        spacing={1}
        style={{ marginTop: "20px", marginBottom: "20px" }}
      >
        <Grid item xs={6}>
          <Button color="success" variant="contained" onClick={handleOpen}>
            Create
          </Button>
        </Grid>
        <Grid item xs={6}></Grid>
      </Grid>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
          <TableBody>
            {(rowsPerPage > 0
              ? suppliersResults.slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                )
              : suppliersResults
            ).map((row, i) => (
              <TableRow key={row.name +"-"+ i}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="center">{row.internalNumber}</TableCell>
                <TableCell align="center">{row.address}</TableCell>
                <TableCell align="right">
                  <IconButton aria-label="delete" onClick={()=> deleteItem(row.name)}>
                    <DeleteIcon color="error"/>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                colSpan={3}
                count={suppliersResults.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: {
                    "aria-label": "rows per page",
                  },
                  native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Supplier</DialogTitle>
        <DialogContent style={{paddingTop: '20px'}}> 
          <form>
            <Grid container spacing={1}>
              <Grid item xs={12} md={6}>
                <TextField
                  value={newSupplier.name}
                  onChange={handleInputChange}
                  id="outlined-basic-name"
                  label="Name"
                  variant="outlined"
                  name="name"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  value={newSupplier.internalNumber}
                  name="internalNumber"
                  onChange={handleInputChange}
                  id="outlined-basic-number"
                  label="Internal Number "
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  name="address"
                  value={newSupplier.adress}
                  onChange={handleInputChange}
                  id="outlined-basic-address"
                  label="Address"
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleAddSupplier} color="primary">
            Add Supplier
          </Button>
        </DialogActions>
      </Dialog>
      {successToast &&
      <Alert severity="success">
        <AlertTitle>Success</AlertTitle>
        Success <strong>check it out!</strong>
      </Alert>
      }
      {errorToast &&
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        Error <strong>check it out!</strong>
      </Alert>
      }
      
    </>
  );
}
