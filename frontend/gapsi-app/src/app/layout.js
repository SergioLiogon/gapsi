"use client";
import { Inter } from 'next/font/google'
import './globals.css'
import Navbar from './component/Navbar'
import { useEffect, useState } from 'react'
import axios from 'axios'
const inter = Inter({ subsets: ['latin'] })
const links = [{ 
  label: 'Home',
  route: '/'
}, {
  label: 'suppliers',
  route: 'supplier'
}]

useEffect
export default function RootLayout({ children }) {

  return (
    <html lang="en">
      <body >
        <nav>
          <Navbar/>
        </nav>
        <main>
          {children}
        </main>
      </body>
    </html>
  )
}
