"use client";
import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";
import Link from "next/link";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import axios from "axios";
export const Greeting = () => {
  const [meta, setMeta] = useState();
  useEffect(() => {
    fetchMeta();
  }, []);

  const fetchMeta = async () => {
    try {
      const response = await axios.get("http://localhost:9898/meta");
      setMeta(response.data);
    } catch (error) {
      console.error;
    }
  };
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "90vh",
        }}
      >
        <Card sx={{ maxWidth: 345, minWidth: 345 }}>
          <CardMedia
            sx={{ height: 300 }}
            image="/static/photo.avif"
            title="green iguana"
          />
          <CardContent>
            {meta && (
              <Typography variant="h5" color="text.secondary">
                Bienvenido candidato {meta?.user}
              </Typography>
            )}
          </CardContent>
          <CardActions>
            <Button component={Link} href="/suppliers" size="small">
              Continuar
            </Button>
          </CardActions>
        </Card>
      </Box>
      <footer>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {meta && (
              <Typography variant="h5" color="text.secondary">
                Version {meta?.version}
              </Typography>
            )}
          </Grid>
        </Grid>
      </footer>
    </>
  );
};
